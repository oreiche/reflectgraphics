\RequirePackage{kvoptions, keyval, graphicx, calc, tikz}
\usetikzlibrary{fadings}

\makeatletter

% Holds the debug condition provided by option \option{debug}.
\newif\ifrg@debug
% Holds the width length provided by option \option{width}.
\newlength\rg@width
% Holds the height length provided by option \option{height}.
\newlength\rg@height
% Holds the distance length provided by option \option{distance}.
\newlength\rg@distance
% Holds the scale factor provided by option \option{scale}.
\def\rg@scale{}
% Holds the angle value provided by option \option{angle}.
\def\rg@angle{}
% Holds the length factor for the reflection provided by option \option{length}.
\def\rg@length{}
% Holds the opacity factor for starting the reflection provided by option
% \option{opacity}.
\def\rg@opacity{}

% \noindent Hold the trim lengths provided by option \option{trim}:

% Left trim length
\newlength\rg@triml
% Bottom trim length
\newlength\rg@trimb
% Right trim length
\newlength\rg@trimr
% Top trim length
\newlength\rg@trimt

% Dimension holding the default unit for converting dimensions.
\newdimen\rg@unit \rg@unit=1bp%
% Dimension temporarily used for storing converted dimensions.
\newdimen\rg@trim@dim%

% Macro for converting an argument without unit to dimension with default unit.
\def\rg@convtodim#1#2{%
  % Set unit variable to default unit.
  \let\rg@unit@cur\rg@unit%
  % Macro for clearing the unit variable.
  \def\rg@unit@off{\let\rg@unit@cur\relax}%
  % Clear unit variable after next assignment.
  \afterassignment\rg@unit@off%
  % Assign argument |#2| to dimension.
  \rg@trim@dim#2\rg@unit@cur%
  % Copy resulting dimension to argument |#1|.
  \setlength#1{\the\rg@trim@dim}%
}

% Macro for parsing trim values from single argument to |\rg@trim[lbrt]| and
% convert to dimension if no unit is specified.
\def\rg@trim@parse#1 #2 #3 #4 #5\\{%
  \rg@convtodim{\rg@triml}{#1}%
  \rg@convtodim{\rg@trimb}{#2}%
  \rg@convtodim{\rg@trimr}{#3}%
  \rg@convtodim{\rg@trimt}{#4}%
}

% Macro for checking whether value is in range or not.
\def\rg@checkval#1#2#3#4{%
  \ifdim#1pt<#3pt%
    \PackageError{reflectgraphics}{%
      The specified #2 value '#1' is less than #3%
    }{%
      Specify the #2 value within the interval [#3,#4].%
    }%
  \else\ifdim#1pt>#4pt%
    \PackageError{reflectgraphics}{%
      The specified #2 value '#1' is greater than #4%
    }{%
      Specify the #2 value within the interval [#3,#4].%
    }%
  \fi\fi%
}

% Sets debug condition.
\define@key{reflectgraphics}{debug}[true]{%
  \csname rg@debug\ifx\relax#1\relax true\else#1\fi\endcsname}
% Sets width length.
\define@key{reflectgraphics}{width}{%
  \setlength\rg@width{#1}}
% Sets height length.
\define@key{reflectgraphics}{height}{%
  \setlength\rg@height{#1}}
% Sets scale factor.
\define@key{reflectgraphics}{scale}{%
  \def\rg@scale{#1}}
% Sets angle factor.
\define@key{reflectgraphics}{angle}{%
  \def\rg@angle{#1}}
% Sets trim lengths.
\define@key{reflectgraphics}{trim}{%
  \rg@trim@parse#1 \\}
% Accept but ignore option \option{clip}.
\define@key{reflectgraphics}{clip}[true]{}
% Sets reflection distance length.
\define@key{reflectgraphics}{distance}{%
  \setlength\rg@distance{#1}}
% Sets reflection length factor after checking that value is in range.
\define@key{reflectgraphics}{length}{%
  \rg@checkval{#1}{length}{0}{1}%
  \def\rg@length{#1}}
% Sets reflection starting opacity after checking that value is in range.
\define@key{reflectgraphics}{opacity}{%
  \rg@checkval{#1}{opacity}{0}{1}%
  \def\rg@opacity{#1}}

% \noindent Set default values.
\setkeys{reflectgraphics}{%
  debug=false,%
  width=0pt,%
  height=0pt,%
  scale=1.0,%
  angle=0,%
  trim=0 0 0 0,%
  distance=3pt,%
  length=0.5,%
  opacity=0.5%
}

% Process package options by defined keys.
\ProcessKeyvalOptions*

% Holds width of the graphic including extra space when rotated.
\newlength{\rg@grp@width}
% Holds height of the graphic including extra space when rotated.
\newlength{\rg@grp@height}
% Holds height of the reflection including extra space when rotated.
\newlength{\rg@ref@height}
% Holds overall height, which is used for clipping.
\newlength{\rg@clp@height}
% Hold $y$ offset of graphics node, which is used for positioning.
\newlength{\rg@grp@offset}
% Hold $y$ offset of reflections node, which is used for positioning.
\newlength{\rg@ref@offset}

% This is the actual macro doing all the work.
\newcommand{\reflectgraphics}[2][]{%
  % Open new group so global options will not be overridden.
  \begingroup%
  %
  % Load local options from first argument.
  \setkeys{reflectgraphics}{#1}%
  %
  % Determine graphics dimensions. Use |\includegraphics| in \option{draft} mode
  % to do that. If both the \option{width} and \option{height} options are
  % specified, there is nothing that needs to be done here.
  \ifdim\rg@width=0pt\ifdim\rg@height=0pt
    \settowidth{\rg@width}{%
      \includegraphics[%
        draft,%
        scale=\rg@scale,%
        trim={\rg@triml} {\rg@trimb} {\rg@trimr} {\rg@trimt}]{#2}}%
    \settoheight{\rg@height}{%
      \includegraphics[%
        draft,%
        scale=\rg@scale,%
        trim={\rg@triml} {\rg@trimb} {\rg@trimr} {\rg@trimt}]{#2}}%
  \else% if width=0 && height!=0
    \settowidth{\rg@width}{%
      \includegraphics[%
        draft,%
        height=\rg@height,%
        trim={\rg@triml} {\rg@trimb} {\rg@trimr} {\rg@trimt}]{#2}}%
  \fi\else\ifdim\rg@height=0pt% && width!=0
    \settoheight{\rg@height}{%
      \includegraphics[%
        draft,%
        width=\rg@width,%
        trim={\rg@triml} {\rg@trimb} {\rg@trimr} {\rg@trimt}]{#2}}%
  \fi\fi%
  %
  % Determine dimensions of the graphics bounding box if rotation is enabled.
  % Again, |\includegraphics| in \option{draft} mode could be used to calculate
  % the boundary size, but calling |\heightof| results in strange values for
  % $\textrm{\option{angle}}<0$ and $\textrm{\option{angle}}>90$. Therefore,
  % computing the boundary size is done manually using \package{pgfmath}, which
  % turns out to be a little bit slower.
  \ifdim\rg@angle pt=0pt%
    \setlength{\rg@grp@width}{\rg@width}%
    \setlength{\rg@grp@height}{\rg@height}%
  \else%
    % This is totally strange: A simple division gets completely messed up if
    % dividing a length by a larger length. So I need to take care to divide
    % lengths only by smaller ones.
    \ifdim\rg@height>\rg@width%
      \pgfmathsetmacro{\rg@alpha}{atan(\rg@height/\rg@width)}%
    \else%
      \pgfmathsetmacro{\rg@alpha}{atan(1/(\rg@width/\rg@height)}%
    \fi%
    \pgfmathsetmacro{\rg@corner}{abs(cos(\rg@alpha))}%
    \pgfmathsetmacro{\rg@cornera}{abs(cos(\rg@alpha+\rg@angle))}%
    \pgfmathsetmacro{\rg@cornerb}{abs(cos(180-\rg@alpha+\rg@angle))}%
    \pgfmathsetmacro{\rg@scale@x}{%
      max(\rg@cornera/\rg@corner,\rg@cornerb/\rg@corner)}%
    \pgfmathsetmacro{\rg@corner}{abs(sin(\rg@alpha))}%
    \pgfmathsetmacro{\rg@cornera}{abs(sin(\rg@alpha+\rg@angle))}%
    \pgfmathsetmacro{\rg@cornerb}{abs(sin(180-\rg@alpha+\rg@angle))}%
    \pgfmathsetmacro{\rg@scale@y}{%
      max(\rg@cornera/\rg@corner,\rg@cornerb/\rg@corner)}%
    \setlength{\rg@grp@width}{\rg@scale@x\rg@width}%
    \setlength{\rg@grp@height}{\rg@scale@y\rg@height}%
  \fi%
  %
  % Compute height of reflection.
  \setlength{\rg@ref@height}{%
    \rg@length\rg@grp@height}%
  % Compute $y$ offset of the reflection that is used for node positioning. The
  % center of the reflection node is moved to the origin of the coordinate
  % system. This is done, because it seems that fading with a specific size can
  % only be applied in a reasonable way at the origin (with option \option{fit
  % fading} set to \emph{false}).
  \setlength{\rg@ref@offset}{%
    0.5\rg@grp@height-0.5\rg@ref@height}%
  % Compute $y$ offset of the graphics node.
  \setlength{\rg@grp@offset}{%
    0.5\rg@ref@height+\rg@distance+0.5\rg@grp@height}%
  % Compute overall height, which is used for clipping.
  \setlength{\rg@clp@height}{%
    \rg@grp@height+\rg@distance+\rg@ref@height}%
  %
  % The following |\tikzfadingfrompicture| macro does not work with
  % Ti\textit{k}Z externalize. Therefore, if the Ti\textit{k}Z library
  % \package{external} is loaded it must be disabled for the whole rendering.
  \ifdefined\tikzexternaldisable%
    \tikzexternaldisable%
  \fi%
  %
  % Define custom fading \option{fade south}, which starts at $0$\,\%
  % transparency and ends at $100$\,\%. The height of this fading equates to the
  % height of the reflection. |\tikzfadingfrompicture| is used instead of
  % |\tikzfading| because of its possibility to define shades with specific
  % sizes. By doing so, this fading is not stretched across the whole node and
  % therefore it can be used to hide a specific part of the reflection node.
  \begin{tikzfadingfrompicture}[name=fade south]%
    \shade[top color=transparent!0, bottom color=transparent!100]%
      (0,0) rectangle (\rg@grp@width,\rg@ref@height);%
  \end{tikzfadingfrompicture}%
  %
  % Start the actual rendering.
  \begin{tikzpicture}%
    % Draw help lines if \option{debug} option is specified.
    \ifrg@debug%
      \def\rg@padding{0.25\rg@grp@height}%
      \draw[help lines]%
        (-0.5\rg@grp@width-\rg@padding,%
          -0.5\rg@ref@height-\rg@distance-\rg@grp@height-\rg@padding)%
        grid (0.5\rg@grp@width+\rg@padding,%
          0.5\rg@ref@height+\rg@distance+\rg@grp@height+\rg@padding);%
    \fi%
    %
    % Clip the following nodes to graphics width |\rg@grp@width| and clipping
    % height |\rg@clp@height|. This is necessary to get rid of the remaining
    % transparent part of the reflection node, which still consumes much space,
    % even though nothing is visible.
    \clip {(-0.5\rg@grp@width,\rg@clp@height-0.5\rg@ref@height)%
      rectangle ++(\rg@grp@width,-\rg@clp@height)};%
    %
    % Draw the graphics node.
    \node at (0,\rg@grp@offset) {%
      % Draw graphics using |\includegraphics|.
      \includegraphics[%
        width=\rg@width,%
        height=\rg@height,%
        angle=\rg@angle,%
        trim={\rg@triml} {\rg@trimb} {\rg@trimr} {\rg@trimt},%
        clip]{#2}%
    };%
    %
    % Draw the reflection node with previously defined custom fading
    % \option{fade south} and with the opacity value specified by option
    % \option{opacity}.
    \node at (0,-\rg@ref@offset) [%
      opacity=\rg@opacity,%
      scope fading=fade south,%
      fit fading=false%
    ] {%
      % Flip reflection node content vertically.
      \scalebox{1}[-1]{%
        % Draw graphics using |\includegraphics|.
        \includegraphics[%
          width=\rg@width,%
          height=\rg@height,%
          angle=\rg@angle,%
          trim={\rg@triml} {\rg@trimb} {\rg@trimr} {\rg@trimt},%
          clip]{#2}%
      }%
    };%
  \end{tikzpicture}%
  %
  % Close group.
  \endgroup%
}

\makeatother

% vim: fileencoding=iso-8859-2
