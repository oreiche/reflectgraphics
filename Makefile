latexmk=latexmk -pdf -latexoption="-interaction=errorstopmode"
pdflatex=pdflatex
generator=sty2dtx

package=reflectgraphics

.PHONY: all package clean distclean
.PRECIOUS: %.dtx %.ins

all: package

package: $(package).pdf
	@mkdir -p $(package)
	@mv $(package).pdf $(package).dtx $(package).ins $(package)
	@cp README example.jpg $(package)

%.pdf: %.sty force
	@$(latexmk) $(@:.pdf=.dtx)

%.sty: %.dtx force
	@$(pdflatex) $(@:.sty=.ins)

%.dtx: force
	@$(generator) -F doc/sty2dtx.cfg -t doc/sty2dtx.tmpl src/$(@:.dtx=.sty) $@

clean:
	rm -f *.aux *.fdb_latexmk *.fls *.glo *.ilg *.ind *.log *.sty

distclean: clean
	rm -rf $(package)

force:
	

